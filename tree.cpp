#include "htree.h"

int main()
{
    BinaryTreeSearch<int> bts;

    int arr[7] = {4,-6 ,9,8,-2,-7,6};

    for(int i = 0; i < 7; i++)
    {
        std::cout <<"[" <<i<<"]";
        bts.push(arr[i]);
        std::cout <<"\n"; 
    }
    
    bts.print();

    std::cout << "\n";

    bool  a = bts.search(6);

    if(a)
        std::cout << "It's in tree\n";
    else
        std::cout << "No its tree\n";
    
    bts.print();

}