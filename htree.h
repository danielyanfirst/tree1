#include <iostream>

template <typename T>
class Node
{
public:
    
    Node* left;
    Node* right;
    T   value;
    
    Node(const T& d) 
    {
        value = d;
        right = nullptr;
        left = nullptr;
    }
};

template <typename T>
class BinaryTreeSearch
{
private:
    Node<T>* head;

public:
    BinaryTreeSearch()   
    {
        std::cout << "Constructor\n";
        head = nullptr;
    }
    
    
   


    void push(const T&   value);
    bool search(const T&   value);


    bool find(const T&);

    void print();

   
};



template <typename T>
void BinaryTreeSearch<T>::push(const T& value)
{   
    Node<T>* tmp = new Node<T>(value);
    Node<T>* current = head;

    if(head == nullptr)
    {   
        std::cout << "Push head";
        head = tmp;
    }
    else
    {   while(current != nullptr)
        {        
          
            if(tmp->value > current->value)
            {   
                std::cout << "Push1";
                if(current->right == nullptr)
                {
                    std::cout << "Push2";
                    current->right = tmp;
                    break;

                }
                current = current->right;
            }
            else
            {
                std::cout << "Push3";
                if(current->left == nullptr)
                {
                    std::cout << "Push4";
                    current->left = tmp;
                    break;
                }
                current = current->left;
            }

        }

        current = tmp;
    }

    
}



template <typename T>
void print_inorder(Node<T>* node)
{
    if (node != nullptr)
    {
        print_inorder(node->left);
        std::cout << node->value << " ";
        print_inorder(node->right);
    }
}

template <typename T>
void BinaryTreeSearch<T>::print()
{
    print_inorder(head);
}

template <typename T>
bool BinaryTreeSearch<T>::search(const T& val)
{
    Node<T>* tmp = head;

    while(tmp != nullptr)
    {
        
    
        if(tmp->value == val)
        {
            return 1;
        }
        else if(tmp->value > val)
        {
            tmp = tmp->left;
        }
        else
        {
            tmp = tmp->right;
        }
    }
    
    return 0;
}
